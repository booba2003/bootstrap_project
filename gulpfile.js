/**
 * Created by booba2003 on 12.11.2015.
 */

var gulp = require('gulp');
var jade = require('gulp-jade');
var less = require('gulp-less');
var path = require('path');
var plumber = require('gulp-plumber');

var paths = {
    'templates': ['./lib/*.jade'],
    'less': ['./lib/less/*.less'],
    'fonts': ['./bower_components/bootstrap/dist/fonts/*'],
    'js': [
        './bower_components/bootstrap/dist/js/bootstrap.min.js',
        './lib/js/*.js'
    ],
};

gulp.task('templates', function () {
    var YOUR_LOCALS = {};
    gulp.src(paths.templates)
        .pipe(plumber())
        .pipe(jade({
            locals: YOUR_LOCALS
        }))
        .pipe(gulp.dest('./dist/'))
});


gulp.task('less', function () {
    return gulp.src(paths.less)
        .pipe(plumber())
        .pipe(less({
            paths: [path.join(__dirname, 'less', 'includes')]
        }))
        .pipe(gulp.dest('./dist/css'));
});

gulp.task('js', function () {
    gulp.src(paths.js)
        .pipe(plumber())
        .pipe(gulp.dest('./dist/js'));
});

gulp.task('fonts', function () {
    gulp.src(paths.fonts)
        .pipe(plumber())
        .pipe(gulp.dest('./dist/fonts'));
});

gulp.task('default', ['templates', 'less', 'js', 'fonts']);

gulp.task('watch', function () {
    gulp.watch(paths.templates, ['templates']);
    gulp.watch(paths.less, ['less']);
    gulp.watch(paths.js, ['js']);
    gulp.watch(paths.fonts, ['fonts']);
});